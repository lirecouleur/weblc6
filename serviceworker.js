/**
 * https://lesdieuxducode.com/blog/2018/8/rendre-un-site-disponible-hors-ligne-avec-le-service-worker
 * https://github.com/mdn/dom-examples/tree/main/service-worker/simple-service-worker
 * voir aussi : https://www.ankursheel.com/blog/programmatically-remove-service-worker
 * http://mobibot.io/blog/1162/cest-quoi-un-service-worker-petite-definition.html
 */

const addResourcesToCache = async (resources) => {
    const cache = await caches.open('cachelc6');
    for (const resource of resources) {
        try {
            const response = await fetch(resource);
            if (!response.ok) {
                throw new Error(`Failed to fetch ${resource}: ${response.status}`);
            }
            await cache.put(resource, response);
        } catch (error) {
            console.error('Failed to add resource to cache:', error);
        }
    }
};

const putInCache = async (request, response) => {
    const cache = await caches.open('cachelc6');
    await cache.put(request, response);
};

const cacheFirst = async ({ request, preloadResponsePromise, fallbackUrl }) => {
    // First try to get the resource from the cache
    const responseFromCache = await caches.match(request);
    if (responseFromCache) {
        return responseFromCache;
    }

    // Next try to use the preloaded response, if it's there
    const preloadResponse = await preloadResponsePromise;
    if (preloadResponse) {
        // console.info('using preload response', preloadResponse);
        putInCache(request, preloadResponse.clone());
        return preloadResponse;
    }

    // Next try to get the resource from the network
    try {
        const responseFromNetwork = await fetch(request);
        // response may be used only once
        // we need to save clone to put one copy in cache
        // and serve second one
        putInCache(request, responseFromNetwork.clone());
        return responseFromNetwork;
    } catch (error) {
        const fallbackResponse = await caches.match(fallbackUrl);
        if (fallbackResponse) {
            return fallbackResponse;
        }
        // when even the fallback response is not available,
        // there is nothing we can do, but we must always
        // return a Response object
        return new Response('Network error happened', {
            status: 408,
            headers: { 'Content-Type': 'text/plain' },
        });
    }
};

const enableNavigationPreload = async () => {
    if (self.registration.navigationPreload) {
        // Enable navigation preloads!
        await self.registration.navigationPreload.enable();
        // self.clients.claim();
        // console.log("service worker activated");
    }
};

self.addEventListener('activate', (event) => {
    event.waitUntil(enableNavigationPreload());
});

self.addEventListener('install', (event) => {
    event.waitUntil(
        addResourcesToCache([
            './LICENSE',
            './index.html',
            './css/lirecouleur/entete-contenu.css',
            './css/lirecouleur/page-ecrire.css',
            './css/lirecouleur/page-lire.css',
            './css/lirecouleur/sidemenu.css',
            './css/globals.css',
            './css/lirecouleur.css',
            './css/navbar.css',
            './fonts/Accessible DfA Regular.ttf',
            './fonts/Andika-Regular.woff',
            './fonts/Luciole-Regular.ttf',
            './fonts/OpenDyslexic-Regular.woff',
            './img/about.svg',
            './img/arc.png',
            './img/arc.svg',
            './img/clear.svg',
            './img/copy.png',
            './img/download.png',
            './img/edit.png',
            './img/favicon.ico',
            './img/gear.svg',
            './img/hamburger.png',
            './img/hamburger.svg',
            './img/icon.png',
            './img/image_declaration.jpg',
            './img/lc6.svg',
            './img/minus.png',
            './img/minus.svg',
            './img/offline.svg',
            './img/open.png',
            './img/paste.png',
            './img/pause.svg',
            './img/play.svg',
            './img/plus.png',
            './img/plus.svg',
            './img/poubelle.png',
            './img/preview.png',
            './img/qrcode.png',
            './img/share.svg',
            './img/www.svg',
            './js/lirecouleur/functionlc6.js',
            './js/lirecouleur/module.js',
            './js/lirecouleur/processlc6.js',
            './js/lirecouleur/uicontrol.js',
            './js/lirecouleur/userprofile.js',
            './libs/js/html5-qrcode.min.js',
            './libs/js/jszip.min.js',
            './libs/js/pdf.min.js',
            './libs/js/pdf.sandbox.min.js',
            './libs/js/pdf.worker.entry.min.js',
            './libs/js/pdf.worker.min.js',
            './libs/css/fr-fr.js',
            './libs/css/toastui-editor.min.css',
            './libs/js/toastui-editor-all.min.js',
            './libs/js/showdown.min.js',
            './libs/js/showdown.min.js.map',
        ])
    );
    // console.log("service worker installed");
});


self.addEventListener('fetch', (event) => {
    event.respondWith(
        (async () => {
            // Attendez que la promesse preloadResponse se résolve
            const preloadResponse = await event.preloadResponse;

            return cacheFirst({
                request: event.request,
                preloadResponsePromise: preloadResponse,
                fallbackUrl: './img/icon.png',
            });
        })()
    );
});
