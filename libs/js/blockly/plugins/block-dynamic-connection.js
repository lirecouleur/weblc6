/* source : https://www.npmjs.com/package/@blockly/block-dynamic-connection */
!(function (t, e) {
  if ("object" == typeof exports && "object" == typeof module)
    module.exports = e(require("blockly/core"));
  else if ("function" == typeof define && define.amd)
    define(["blockly/core"], e);
  else {
    var n =
      "object" == typeof exports ? e(require("blockly/core")) : e(t.Blockly);
    for (var i in n) ("object" == typeof exports ? exports : t)[i] = n[i];
  }
})(this, (t) =>
  (() => {
    "use strict";
    var e = {
        370: (e) => {
          e.exports = t;
        },
      },
      n = {};
    function i(t) {
      var s = n[t];
      if (void 0 !== s) return s.exports;
      var o = (n[t] = { exports: {} });
      return e[t](o, o.exports, i), o.exports;
    }
    (i.d = (t, e) => {
      for (var n in e)
        i.o(e, n) &&
          !i.o(t, n) &&
          Object.defineProperty(t, n, { enumerable: !0, get: e[n] });
    }),
      (i.o = (t, e) => Object.prototype.hasOwnProperty.call(t, e)),
      (i.r = (t) => {
        "undefined" != typeof Symbol &&
          Symbol.toStringTag &&
          Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
          Object.defineProperty(t, "__esModule", { value: !0 });
      });
    var s = {};
    return (
      (() => {
        i.r(s),
          i.d(s, {
            blockIsDynamic: () => l,
            decoratePreviewer: () => r,
            finalizeConnections: () => a,
            overrideOldBlockDefinitions: () => u,
          });
        var t = i(370);
        const e = {
          minInputs: 1,
          elseifCount: 0,
          elseCount: 0,
          init() {
            this.setHelpUrl(t.Msg.CONTROLS_IF_HELPURL),
              this.setStyle("logic_blocks"),
              this.addFirstCase(),
              this.setNextStatement(!0),
              this.setPreviousStatement(!0),
              this.setTooltip(t.Msg.LISTS_CREATE_WITH_TOOLTIP);
          },
          mutationToDom() {
            if (
              (this.isDeadOrDying() ||
                (t.Events.disable(),
                this.finalizeConnections(),
                this instanceof t.BlockSvg && this.initSvg(),
                t.Events.enable()),
              !this.elseifCount && !this.elseCount)
            )
              return null;
            const e = t.utils.xml.createElement("mutation");
            return (
              this.elseifCount &&
                e.setAttribute("elseif", `${this.elseifCount}`),
              this.elseCount && e.setAttribute("else", "1"),
              e
            );
          },
          domToMutation(t) {
            t.getAttribute("inputs")
              ? this.deserializeInputs(t)
              : this.deserializeCounts(t);
          },
          deserializeInputs(e) {
            const n = e.getAttribute("inputs");
            if (n) {
              const e = n.split(",");
              this.getInput("IF0") && this.removeInput("IF0"),
                this.getInput("DO0") && this.removeInput("DO0");
              const i = e[0];
              this.appendValueInput("IF" + i)
                .setCheck("Boolean")
                .appendField(t.Msg.CONTROLS_IF_MSG_IF, "if"),
                this.appendStatementInput("DO" + i).appendField(
                  t.Msg.CONTROLS_IF_MSG_THEN
                );
              for (let n = 1; n < e.length; n++)
                this.appendValueInput("IF" + e[n])
                  .setCheck("Boolean")
                  .appendField(t.Msg.CONTROLS_IF_MSG_ELSEIF, "elseif"),
                  this.appendStatementInput("DO" + e[n]).appendField(
                    t.Msg.CONTROLS_IF_MSG_THEN
                  );
            }
            "true" == e.getAttribute("else") && this.addElseInput();
          },
          deserializeCounts(t) {
            var e, n;
            (this.elseifCount =
              parseInt(
                null !== (e = t.getAttribute("elseif")) && void 0 !== e
                  ? e
                  : "0",
                10
              ) || 0),
              (this.elseCount =
                parseInt(
                  null !== (n = t.getAttribute("else")) && void 0 !== n
                    ? n
                    : "0",
                  10
                ) || 0);
            for (let t = 1; t <= this.elseifCount; t++)
              this.insertElseIf(this.inputList.length, t);
            this.elseCount && this.addElseInput();
          },
          saveExtraState: function () {
            if (
              (this.isDeadOrDying() ||
                this.isCorrectlyFormatted() ||
                (t.Events.disable(),
                this.finalizeConnections(),
                this instanceof t.BlockSvg && this.initSvg(),
                t.Events.enable()),
              !this.elseifCount && !this.elseCount)
            )
              return null;
            const e = Object.create(null);
            return (
              this.elseifCount && (e.elseIfCount = this.elseifCount),
              this.elseCount && (e.hasElse = !0),
              e
            );
          },
          loadExtraState: function (e) {
            if ("string" != typeof e) {
              (this.elseifCount = e.elseIfCount || 0),
                (this.elseCount = e.hasElse ? 1 : 0);
              for (let t = 1; t <= this.elseifCount; t++)
                this.insertElseIf(this.inputList.length, t);
              this.elseCount && this.addElseInput();
            } else this.domToMutation(t.utils.xml.textToDom(e));
          },
          findInputIndexForConnection(t) {
            var e;
            if (
              !t.targetConnection ||
              (null === (e = t.targetBlock()) || void 0 === e
                ? void 0
                : e.isInsertionMarker())
            )
              return null;
            for (let e = 0; e < this.inputList.length; e++)
              if (this.inputList[e].connection == t) return e;
            return null;
          },
          insertElseIf(e, n) {
            const i = this.appendValueInput("IF" + n)
                .setCheck("Boolean")
                .appendField(t.Msg.CONTROLS_IF_MSG_ELSEIF, "elseif"),
              s = this.appendStatementInput("DO" + n).appendField(
                t.Msg.CONTROLS_IF_MSG_THEN
              );
            return (
              this.moveInputBefore("IF" + n, this.inputList[e].name),
              this.moveInputBefore("DO" + n, this.inputList[e + 1].name),
              { ifInput: i, doInput: s }
            );
          },
          onPendingConnection(e) {
            e.type !== t.NEXT_STATEMENT ||
              this.getInput("ELSE") ||
              this.addElseInput();
            const n = this.findInputIndexForConnection(e);
            if (null !== n && this.inputList[n].name.includes("IF")) {
              const e = this.inputList[n + 2];
              if (e && "ELSE" != e.name) {
                const i = e && e.connection && e.connection.targetConnection;
                i &&
                  !i.getSourceBlock().isInsertionMarker() &&
                  this.insertElseIf(n + 2, t.utils.idGenerator.genUid());
              } else this.insertElseIf(n + 2, t.utils.idGenerator.genUid());
            }
          },
          finalizeConnections() {
            var t, e, n;
            const i = this.collectTargetCaseConns(),
              s =
                null ===
                  (e =
                    null === (t = this.getInput("ELSE")) || void 0 === t
                      ? void 0
                      : t.connection) || void 0 === e
                  ? void 0
                  : e.targetConnection;
            this.tearDownBlock(),
              this.addFirstCase(),
              this.addCaseInputs(i),
              s &&
                (null === (n = this.addElseInput().connection) ||
                  void 0 === n ||
                  n.connect(s)),
              (this.elseifCount = Math.max(i.length - 1, 0)),
              (this.elseCount = s ? 1 : 0);
          },
          collectTargetCaseConns() {
            var t, e;
            const n = [];
            for (let i = 0; i < this.inputList.length - 1; i += 2) {
              const s =
                  null === (t = this.inputList[i].connection) || void 0 === t
                    ? void 0
                    : t.targetConnection,
                o =
                  null === (e = this.inputList[i + 1].connection) ||
                  void 0 === e
                    ? void 0
                    : e.targetConnection;
              (s || o) && n.push({ ifTarget: s, doTarget: o });
            }
            return n;
          },
          tearDownBlock() {
            for (let t = this.inputList.length - 1; t >= 0; t--)
              this.removeInput(this.inputList[t].name);
          },
          addCaseInputs(t) {
            var e, n;
            for (let i = 0; i < t.length; i++) {
              let s = this.getInput(`IF${i}`),
                o = this.getInput(`DO${i}`);
              (s && o) ||
                ({ ifInput: s, doInput: o } = this.insertElseIf(2 * i, i));
              const { ifTarget: l, doTarget: r } = t[i];
              l &&
                (null === (e = s.connection) || void 0 === e || e.connect(l)),
                r &&
                  (null === (n = o.connection) || void 0 === n || n.connect(r));
            }
          },
          addElseInput() {
            return this.appendStatementInput("ELSE").appendField(
              t.Msg.CONTROLS_IF_MSG_ELSE
            );
          },
          addFirstCase() {
            this.appendValueInput("IF0")
              .setCheck("Boolean")
              .appendField(t.Msg.CONTROLS_IF_MSG_IF, "if"),
              this.appendStatementInput("DO0").appendField(
                t.Msg.CONTROLS_IF_MSG_THEN
              );
          },
          isCorrectlyFormatted() {
            for (let t = 0; t < this.inputList.length - 1; t += 2) {
              if (this.inputList[t].name !== `IF${t}`) return !1;
              if (this.inputList[t + 1].name !== `DO${t}`) return !1;
            }
            return !0;
          },
        };
        t.Blocks.dynamic_if = e;
        const n = {
          minInputs: 2,
          itemCount: 0,
          init() {
            (this.itemCount = this.minInputs),
              this.setHelpUrl(t.Msg.TEXT_JOIN_HELPURL),
              this.setStyle("text_blocks"),
              this.addFirstInput();
            for (let t = 1; t < this.minInputs; t++)
              this.appendValueInput(`ADD${t}`);
            this.setOutput(!0, "String"),
              this.setTooltip(t.Msg.TEXT_JOIN_TOOLTIP);
          },
          mutationToDom() {
            this.isDeadOrDying() ||
              (t.Events.disable(),
              this.finalizeConnections(),
              this instanceof t.BlockSvg && this.initSvg(),
              t.Events.enable());
            const e = t.utils.xml.createElement("mutation");
            return e.setAttribute("items", `${this.itemCount}`), e;
          },
          domToMutation(t) {
            t.getAttribute("inputs")
              ? this.deserializeInputs(t)
              : this.deserializeCounts(t);
          },
          deserializeInputs(e) {
            const n = e.getAttribute("inputs");
            if (n) {
              const e = n.split(",");
              (this.inputList = []),
                e.forEach((t) => this.appendValueInput(t)),
                this.inputList[0].appendField(t.Msg.TEXT_JOIN_TITLE_CREATEWITH);
            }
          },
          deserializeCounts(t) {
            var e;
            this.itemCount = Math.max(
              parseInt(
                null !== (e = t.getAttribute("items")) && void 0 !== e
                  ? e
                  : "0",
                10
              ),
              this.minInputs
            );
            for (let t = this.minInputs; t < this.itemCount; t++)
              this.appendValueInput("ADD" + t);
          },
          saveExtraState: function () {
            return (
              this.isDeadOrDying() ||
                this.isCorrectlyFormatted() ||
                (t.Events.disable(),
                this.finalizeConnections(),
                this instanceof t.BlockSvg && this.initSvg(),
                t.Events.enable()),
              { itemCount: this.itemCount }
            );
          },
          loadExtraState: function (e) {
            var n;
            if ("string" != typeof e) {
              this.itemCount =
                null !== (n = e.itemCount) && void 0 !== n ? n : 0;
              for (let t = this.minInputs; t < this.itemCount; t++)
                this.appendValueInput("ADD" + t);
            } else this.domToMutation(t.utils.xml.textToDom(e));
          },
          findInputIndexForConnection(t) {
            var e, n;
            if (
              !t.targetConnection ||
              (null === (e = t.targetBlock()) || void 0 === e
                ? void 0
                : e.isInsertionMarker())
            )
              return null;
            let i = -1;
            for (let e = 0; e < this.inputList.length; e++)
              this.inputList[e].connection == t && (i = e);
            if (i == this.inputList.length - 1)
              return this.inputList.length + 1;
            const s = this.inputList[i + 1],
              o =
                null === (n = null == s ? void 0 : s.connection) || void 0 === n
                  ? void 0
                  : n.targetConnection;
            return o && !o.getSourceBlock().isInsertionMarker() ? i + 1 : null;
          },
          onPendingConnection(e) {
            const n = this.findInputIndexForConnection(e);
            null != n &&
              (this.appendValueInput(`ADD${t.utils.idGenerator.genUid()}`),
              this.moveNumberedInputBefore(this.inputList.length - 1, n));
          },
          finalizeConnections() {
            const t = this.removeUnnecessaryEmptyConns(
              this.inputList.map((t) => {
                var e;
                return null === (e = t.connection) || void 0 === e
                  ? void 0
                  : e.targetConnection;
              })
            );
            this.tearDownBlock(),
              this.addItemInputs(t),
              (this.itemCount = t.length);
          },
          tearDownBlock() {
            for (let t = this.inputList.length - 1; t >= 0; t--)
              this.removeInput(this.inputList[t].name);
          },
          removeUnnecessaryEmptyConns(t) {
            const e = [...t];
            for (let t = e.length - 1; t >= 0; t--)
              !e[t] && e.length > this.minInputs && e.splice(t, 1);
            return e;
          },
          addItemInputs(t) {
            var e, n;
            const i = this.addFirstInput(),
              s = t[0];
            s && (null === (e = i.connection) || void 0 === e || e.connect(s));
            for (let e = 1; e < t.length; e++) {
              const i = this.appendValueInput(`ADD${e}`),
                s = t[e];
              s &&
                (null === (n = i.connection) || void 0 === n || n.connect(s));
            }
          },
          addFirstInput() {
            return this.appendValueInput("ADD0").appendField(
              t.Msg.TEXT_JOIN_TITLE_CREATEWITH
            );
          },
          isCorrectlyFormatted() {
            for (let t = 0; t < this.inputList.length; t++)
              if (this.inputList[t].name !== `ADD${t}`) return !1;
            return !0;
          },
        };
        t.Blocks.dynamic_text_join = n;
        const o = {
          minInputs: 2,
          itemCount: 0,
          init() {
            (this.itemCount = this.minInputs),
              this.setHelpUrl(t.Msg.LISTS_CREATE_WITH_HELPURL),
              this.setStyle("list_blocks"),
              this.addFirstInput();
            for (let t = 1; t < this.minInputs; t++)
              this.appendValueInput(`ADD${t}`);
            this.setOutput(!0, "Array"),
              this.setTooltip(t.Msg.LISTS_CREATE_WITH_TOOLTIP);
          },
          mutationToDom() {
            this.isDeadOrDying() ||
              (t.Events.disable(),
              this.finalizeConnections(),
              this instanceof t.BlockSvg && this.initSvg(),
              t.Events.enable());
            const e = t.utils.xml.createElement("mutation");
            return e.setAttribute("items", `${this.itemCount}`), e;
          },
          domToMutation(t) {
            t.getAttribute("inputs")
              ? this.deserializeInputs(t)
              : this.deserializeCounts(t);
          },
          deserializeInputs(e) {
            const n = e.getAttribute("inputs");
            if (n) {
              const e = n.split(",");
              (this.inputList = []),
                e.forEach((t) => this.appendValueInput(t)),
                this.inputList[0].appendField(
                  t.Msg.LISTS_CREATE_WITH_INPUT_WITH
                );
            }
          },
          deserializeCounts(t) {
            var e;
            this.itemCount = Math.max(
              parseInt(
                null !== (e = t.getAttribute("items")) && void 0 !== e
                  ? e
                  : "0",
                10
              ),
              this.minInputs
            );
            for (let t = this.minInputs; t < this.itemCount; t++)
              this.appendValueInput("ADD" + t);
          },
          saveExtraState: function () {
            return (
              this.isDeadOrDying() ||
                this.isCorrectlyFormatted() ||
                (t.Events.disable(),
                this.finalizeConnections(),
                this instanceof t.BlockSvg && this.initSvg(),
                t.Events.enable()),
              { itemCount: this.itemCount }
            );
          },
          loadExtraState: function (e) {
            var n;
            if ("string" != typeof e) {
              this.itemCount =
                null !== (n = e.itemCount) && void 0 !== n ? n : 0;
              for (let t = this.minInputs; t < this.itemCount; t++)
                this.appendValueInput("ADD" + t);
            } else this.domToMutation(t.utils.xml.textToDom(e));
          },
          findInputIndexForConnection(t) {
            var e, n;
            if (
              !t.targetConnection ||
              (null === (e = t.targetBlock()) || void 0 === e
                ? void 0
                : e.isInsertionMarker())
            )
              return null;
            let i = -1;
            for (let e = 0; e < this.inputList.length; e++)
              this.inputList[e].connection == t && (i = e);
            if (i == this.inputList.length - 1)
              return this.inputList.length + 1;
            const s = this.inputList[i + 1],
              o =
                null === (n = null == s ? void 0 : s.connection) || void 0 === n
                  ? void 0
                  : n.targetConnection;
            return o && !o.getSourceBlock().isInsertionMarker() ? i + 1 : null;
          },
          onPendingConnection(e) {
            const n = this.findInputIndexForConnection(e);
            null != n &&
              (this.appendValueInput(`ADD${t.utils.idGenerator.genUid()}`),
              this.moveNumberedInputBefore(this.inputList.length - 1, n));
          },
          finalizeConnections() {
            const t = this.removeUnnecessaryEmptyConns(
              this.inputList.map((t) => {
                var e;
                return null === (e = t.connection) || void 0 === e
                  ? void 0
                  : e.targetConnection;
              })
            );
            this.tearDownBlock(),
              this.addItemInputs(t),
              (this.itemCount = t.length);
          },
          tearDownBlock() {
            for (let t = this.inputList.length - 1; t >= 0; t--)
              this.removeInput(this.inputList[t].name);
          },
          removeUnnecessaryEmptyConns(t) {
            const e = [...t];
            for (let t = e.length - 1; t >= 0; t--)
              !e[t] && e.length > this.minInputs && e.splice(t, 1);
            return e;
          },
          addItemInputs(t) {
            var e, n;
            const i = this.addFirstInput(),
              s = t[0];
            s && (null === (e = i.connection) || void 0 === e || e.connect(s));
            for (let e = 1; e < t.length; e++) {
              const i = this.appendValueInput(`ADD${e}`),
                s = t[e];
              s &&
                (null === (n = i.connection) || void 0 === n || n.connect(s));
            }
          },
          addFirstInput() {
            return this.appendValueInput("ADD0").appendField(
              t.Msg.LISTS_CREATE_WITH_INPUT_WITH
            );
          },
          isCorrectlyFormatted() {
            for (let t = 0; t < this.inputList.length; t++)
              if (this.inputList[t].name !== `ADD${t}`) return !1;
            return !0;
          },
        };
        function l(t) {
          return (
            void 0 !== t.onPendingConnection && void 0 !== t.finalizeConnections
          );
        }
        function r(e) {
          return class {
            constructor(n) {
              this.pendingBlocks = new Set();
              const i = null != e ? e : t.InsertionMarkerPreviewer;
              this.basePreviewer = new i(n);
            }
            previewReplacement(t, e, n) {
              this.previewDynamism(e),
                this.basePreviewer.previewReplacement(t, e, n);
            }
            previewConnection(t, e) {
              this.previewDynamism(e),
                this.basePreviewer.previewConnection(t, e);
            }
            hidePreview() {
              this.basePreviewer.hidePreview();
            }
            dispose() {
              for (const t of this.pendingBlocks) {
                if (t.isDeadOrDying()) return;
                t.finalizeConnections();
              }
              this.pendingBlocks.clear(), this.basePreviewer.dispose();
            }
            previewDynamism(t) {
              const e = t.getSourceBlock();
              l(e) && (e.onPendingConnection(t), this.pendingBlocks.add(e));
            }
          };
        }
        t.Blocks.dynamic_list_create = o;
        const u = function () {
          (t.Blocks.lists_create_with = t.Blocks.dynamic_list_create),
            (t.Blocks.text_join = t.Blocks.dynamic_text_join),
            (t.Blocks.controls_if = t.Blocks.dynamic_if);
        };
        function a(e) {
          var n;
          if (e.type === t.Events.BLOCK_DELETE) {
            const i = t.Workspace.getById(
              null !== (n = e.workspaceId) && void 0 !== n ? n : ""
            );
            if (!i) return;
            for (const t of i.getAllBlocks()) l(t) && t.finalizeConnections();
          }
        }
      })(),
      s
    );
  })()
);
