LireCouleur 6 est essentiellement destiné aux utilisateurs sur un appareil personnel, type smartphone ou tablette. Il a été pensé comme une aide à l'autonomie pour des élèves de niveau cycle 3 ou cycle 4.

### Fonctions principales

Les lettres, graphèmes, syllabes, mots et lignes peuvent être mis en évidence par des alternances de couleurs.

Exemple d'alternance de couleurs sur les mots :

![Alternance de couleurs sur les mots](doc/alternmots.png)

D'autres fonctions sont également disponibles.

**Mise en évidence des syllabes**

-   ajout d'un caractère de séparation entre les syllabes

![Séparation des syllabes](doc/syllabes.png)

-   souligner les syllabes

![Souligner les syllabes](doc/syllarc.png)

**Mise en évidence des graphèmes**

-   attribution d'une typographie particulière à chaque graphème

![Coloriage des graphèmes](doc/graphemes.png)

**Mise en évidence de lettres**

-   attribution de couleurs à certaines lettres ou succession de lettres. Cette fonction est utile en particulier pour limiter les confusions de lettres comme **b**, **d**, **p**, **q** ou pour travailer la différence entre les syllabes comme **pir** et **pri**.

![Coloriage de successions de lettres](doc/lettres.png)

#### Vue par défaut : le texte adapté

![Vue de texte adapté](doc/texte_adapte.png)

Le texte est adapté selon le profil sélectionné dans la liste des profils disponibles.

Le texte peut être **oralisé**. Si une partie du texte est sélectionnée, seule cette partie est lue par la synthèse vocale.

#### Saisie d'un texte à adapter

![Vue de saisie d'un texte](doc/modification_texte.png)

Le texte à adapter peut être :

-   **importé** à partir d'un fichier **texte**, **pdf** ou **docx**
-   **scanné** depuis un QR code
-   **collé** depuis le presse-papier
-   **saisi** dans la zone de texte modifiable en utilisant le clavier

#### Gestion des profils utilisateurs

![Vue de gestion des profils](doc/gestion_profils.png)

Différents profils utilisateurs sont proposés par défaut. Il est possible de supprimer les profils inutiles.

Il est également possible de modifier les profils en utilisant [l'éditeur de profil](https://lirecouleur.forge.apps.education.fr/weblc6/editeur.html).

### Application pédagogique : aide à la lecture de textes numérisés

Il est généralement assez simple de copier un texte numérisé et de le coller dans l'application pour voir directement ce texte adapté selon le profil choisi par l'utilisateur.

LireCouleur 6 permet également d'ouvrir des fichiers au format PDF ou DOCX pour en adapter le contenu.

### Application pédagogique : aide à la lecture de consignes

L'une des première utilisation de LireCouleur 6 a été de proposer des exercices papier dont les consignes ont été insérées dans des QR codes. Les élèves pouvaient alors scanner ces QR codes pour accéder à la version adaptée et à la version oralisée des consignes. Ceci leur permet de demander la relecture de la consigne aussi souvent que nécessaire et décharge l'enseignant ou les accompagnants de cette tâche, tout en aidant les élèves à gagner en autonomie.

### Adaptation pédagogique

LireCouleur 6 a été réfléchi pour simplifier le travail d'adaptation pédagogique souvent nécessaire dans une classe. L'objectif est que l'individualisation de l'adaptation est réalisée sur l'appareil utilisé par un apprenant et que l'enseignant puisse proposer des adaptations génériques.

L'intérêt est que l'enseignant peut, par exemple, fournir le même texte à tous les élèves ou décliner un texte en une ou plusieurs versions simplifiées. Le gain de temps est réel dans la mesure où l'adaptation est mutualisable pour un ensemble d'élèves aux profils d'apprentissage différents.

D'autres utilisations sont bien entendu tout à fait possibles. N'hésitez pas à partager les vôtres pour en faire profiter la communauté.
