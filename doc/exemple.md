# Le petit prince, CHAPITRE 21

![](https://www.ebooksgratuits.com/html/st_exupery_le_petit_prince_fichiers/image040.jpg)

[image issue de https://www.ebooksgratuits.com](https://www.ebooksgratuits.com/html/st_exupery_le_petit_prince.html)

– Je ne puis pas jouer avec toi, dit le **renard**. Je ne suis pas
apprivoisé.

– Ah ! **pardon**, fit le petit **prince**.

Mais, après **réflexion**, il ajouta :

– Qu’est-ce que signifie « **apprivoiser** » ?

– Tu n’es pas d’ici, dit le renard, que cherches-tu ?

– **Je cherche les hommes, dit le petit prince. Qu’est-ce que signifie « apprivoiser » ?**

– **Les hommes, dit le renard, ils ont des fusils et ils chassent. C’est bien gênant !
Ils élèvent aussi des poules. C’est leur seul intérêt. Tu cherches des poules ?**

*Antoine de Saint-Exupéry, Le petit prince, 1943*

---

> Dans ce texte, seules les portions entourées de double étoiles ont été adaptées par LireCouleur.
> Si la balise double étoile n'est pas utilisée dans le texte, tout le texte est adapté par LireCouleur.
> Guide d'écriture Markdown : https://docs.forge.apps.education.fr/tutoriels/tutomd/
