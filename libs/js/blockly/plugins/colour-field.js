/* source : https://www.npmjs.com/package/@blockly/field-colour */
!(function (t, r) {
  if ("object" == typeof exports && "object" == typeof module)
    module.exports = r(
      require("blockly/core"),
      require("blockly/javascript"),
      require("blockly/dart"),
      require("blockly/lua"),
      require("blockly/php"),
      require("blockly/python")
    );
  else if ("function" == typeof define && define.amd)
    define([
      "blockly/core",
      "blockly/javascript",
      "blockly/dart",
      "blockly/lua",
      "blockly/php",
      "blockly/python",
    ], r);
  else {
    var o =
      "object" == typeof exports
        ? r(
            require("blockly/core"),
            require("blockly/javascript"),
            require("blockly/dart"),
            require("blockly/lua"),
            require("blockly/php"),
            require("blockly/python")
          )
        : r(
            t.Blockly,
            t["Blockly.JavaScript"],
            t["Blockly.Dart"],
            t["Blockly.Lua"],
            t["Blockly.PHP"],
            t["Blockly.Python"]
          );
    for (var e in o) ("object" == typeof exports ? exports : t)[e] = o[e];
  }
})(this, (t, r, o, e, n, i) =>
  (() => {
    "use strict";
    var l = {
        370: (r) => {
          r.exports = t;
        },
        379: (t) => {
          t.exports = o;
        },
        127: (t) => {
          t.exports = r;
        },
        157: (t) => {
          t.exports = e;
        },
        537: (t) => {
          t.exports = n;
        },
        557: (t) => {
          t.exports = i;
        },
      },
      s = {};
    function a(t) {
      var r = s[t];
      if (void 0 !== r) return r.exports;
      var o = (s[t] = { exports: {} });
      return l[t](o, o.exports, a), o.exports;
    }
    (a.d = (t, r) => {
      for (var o in r)
        a.o(r, o) &&
          !a.o(t, o) &&
          Object.defineProperty(t, o, { enumerable: !0, get: r[o] });
    }),
      (a.o = (t, r) => Object.prototype.hasOwnProperty.call(t, r)),
      (a.r = (t) => {
        "undefined" != typeof Symbol &&
          Symbol.toStringTag &&
          Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
          Object.defineProperty(t, "__esModule", { value: !0 });
      });
    var c = {};
    return (
      (() => {
        a.r(c),
          a.d(c, {
            FieldColour: () => i,
            colourBlend: () => e,
            colourPicker: () => t,
            colourRandom: () => r,
            colourRgb: () => o,
            installAllBlocks: () => tt,
            registerFieldColour: () => l,
          });
        var t = {};
        a.r(t),
          a.d(t, {
            BLOCK_NAME: () => f,
            blockDefinition: () => L,
            installBlock: () => N,
            toDart: () => b,
            toJavascript: () => p,
            toLua: () => _,
            toPhp: () => C,
            toPython: () => m,
          });
        var r = {};
        a.r(r),
          a.d(r, {
            BLOCK_NAME: () => v,
            blockDefinition: () => I,
            installBlock: () => U,
            toDart: () => T,
            toJavascript: () => $,
            toLua: () => x,
            toPhp: () => k,
            toPython: () => y,
          });
        var o = {};
        a.r(o),
          a.d(o, {
            BLOCK_NAME: () => A,
            blockDefinition: () => G,
            installBlock: () => K,
            toDart: () => S,
            toJavascript: () => D,
            toLua: () => M,
            toPhp: () => P,
            toPython: () => H,
          });
        var e = {};
        a.r(e),
          a.d(e, {
            BLOCK_NAME: () => j,
            blockDefinition: () => Z,
            installBlock: () => Q,
            toDart: () => V,
            toJavascript: () => q,
            toLua: () => J,
            toPhp: () => z,
            toPython: () => W,
          });
        var n = a(370);
        class i extends n.Field {
          constructor(t, r, o) {
            super(n.Field.SKIP_SETUP),
              (this.picker = null),
              (this.highlightedIndex = null),
              (this.boundEvents = []),
              (this.SERIALIZABLE = !0),
              (this.CURSOR = "default"),
              (this.isDirty_ = !1),
              (this.colours = [
                "#ffffff",
                "#cccccc",
                "#c0c0c0",
                "#999999",
                "#666666",
                "#333333",
                "#000000",
                "#ffcccc",
                "#ff6666",
                "#ff0000",
                "#cc0000",
                "#990000",
                "#660000",
                "#330000",
                "#ffcc99",
                "#ff9966",
                "#ff9900",
                "#ff6600",
                "#cc6600",
                "#993300",
                "#663300",
                "#ffff99",
                "#ffff66",
                "#ffcc66",
                "#ffcc33",
                "#cc9933",
                "#996633",
                "#663333",
                "#ffffcc",
                "#ffff33",
                "#ffff00",
                "#ffcc00",
                "#999900",
                "#666600",
                "#333300",
                "#99ff99",
                "#66ff99",
                "#33ff33",
                "#33cc00",
                "#009900",
                "#006600",
                "#003300",
                "#99ffff",
                "#33ffff",
                "#66cccc",
                "#00cccc",
                "#339999",
                "#336666",
                "#003333",
                "#ccffff",
                "#66ffff",
                "#33ccff",
                "#3366ff",
                "#3333ff",
                "#000099",
                "#000066",
                "#ccccff",
                "#9999ff",
                "#6666cc",
                "#6633ff",
                "#6600cc",
                "#333399",
                "#330099",
                "#ffccff",
                "#ff99ff",
                "#cc66cc",
                "#cc33cc",
                "#993399",
                "#663366",
                "#330033",
              ]),
              (this.titles = []),
              (this.columns = 7),
              t !== n.Field.SKIP_SETUP &&
                (o && this.configure_(o),
                this.setValue(t),
                r && this.setValidator(r));
          }
          configure_(t) {
            super.configure_(t),
              t.colourOptions && (this.colours = t.colourOptions),
              t.colourTitles && (this.titles = t.colourTitles),
              t.columns && (this.columns = t.columns);
          }
          initView() {
            const t = this.getConstants();
            if (!t) throw Error("Constants not found");
            (this.size_ = new n.utils.Size(
              t.FIELD_COLOUR_DEFAULT_WIDTH,
              t.FIELD_COLOUR_DEFAULT_HEIGHT
            )),
              this.createBorderRect_(),
              (this.getBorderRect().style.fillOpacity = "1"),
              this.getBorderRect().setAttribute("stroke", "#fff"),
              this.isFullBlockField() &&
                (this.clickTarget_ = this.sourceBlock_.getSvgRoot());
          }
          isFullBlockField() {
            if (!this.getSourceBlock()) throw new n.UnattachedFieldError();
            const t = this.getConstants();
            return (
              this.blockIsSimpleReporter() &&
              Boolean(null == t ? void 0 : t.FIELD_COLOUR_FULL_BLOCK)
            );
          }
          blockIsSimpleReporter() {
            const t = this.getSourceBlock();
            if (!t) throw new n.UnattachedFieldError();
            if (!t.outputConnection) return !1;
            for (const r of t.inputList)
              if (r.connection || r.fieldRow.length > 1) return !1;
            return !0;
          }
          applyColour() {
            const t = this.getSourceBlock();
            if (!t) throw new n.UnattachedFieldError();
            if (!this.fieldGroup_) return;
            const r = this.borderRect_;
            if (!r) throw new Error("The border rect has not been initialized");
            this.isFullBlockField()
              ? ((r.style.display = "none"),
                t.pathObject.svgPath.setAttribute("fill", this.getValue()),
                t.pathObject.svgPath.setAttribute("stroke", "#fff"))
              : ((r.style.display = "block"), (r.style.fill = this.getValue()));
          }
          getSize() {
            var t;
            return (
              (null === (t = this.getConstants()) || void 0 === t
                ? void 0
                : t.FIELD_COLOUR_FULL_BLOCK) &&
                (this.render_(), (this.isDirty_ = !1)),
              super.getSize()
            );
          }
          render_() {
            super.render_();
            const t = this.getSourceBlock();
            if (!t) throw new n.UnattachedFieldError();
            t.applyColour();
          }
          updateSize_(t) {
            const r = this.getConstants();
            if (!r) return;
            let o, e;
            this.isFullBlockField()
              ? ((o = 2 * (null != t ? t : 0)), (e = r.FIELD_TEXT_HEIGHT))
              : ((o = r.FIELD_COLOUR_DEFAULT_WIDTH),
                (e = r.FIELD_COLOUR_DEFAULT_HEIGHT)),
              (this.size_.height = e),
              (this.size_.width = o),
              this.positionBorderRect_();
          }
          doClassValidation_(t) {
            return "string" != typeof t ? null : n.utils.colour.parse(t);
          }
          getText() {
            let t = this.value_;
            return (
              /^#(.)\1(.)\2(.)\3$/.test(t) && (t = "#" + t[1] + t[3] + t[5]), t
            );
          }
          setColours(t, r) {
            return (this.colours = t), r && (this.titles = r), this;
          }
          setColumns(t) {
            return (this.columns = t), this;
          }
          showEditor_() {
            if ((this.dropdownCreate(), !this.picker))
              throw Error("Picker not found");
            n.DropDownDiv.getContentDiv().appendChild(this.picker),
              n.DropDownDiv.showPositionedByField(
                this,
                this.dropdownDispose.bind(this)
              ),
              this.picker.focus({ preventScroll: !0 });
          }
          onClick(t) {
            const r = t.target,
              o = r && r.getAttribute("data-colour");
            null !== o && (this.setValue(o), n.DropDownDiv.hideIfOwner(this));
          }
          onKeyDown(t) {
            let r,
              o = !0;
            switch (t.key) {
              case "ArrowUp":
                this.moveHighlightBy(0, -1);
                break;
              case "ArrowDown":
                this.moveHighlightBy(0, 1);
                break;
              case "ArrowLeft":
                this.moveHighlightBy(-1, 0);
                break;
              case "ArrowRight":
                this.moveHighlightBy(1, 0);
                break;
              case "Enter":
                if (((r = this.getHighlighted()), r)) {
                  const t = r.getAttribute("data-colour");
                  null !== t && this.setValue(t);
                }
                n.DropDownDiv.hideWithoutAnimation();
                break;
              default:
                o = !1;
            }
            o && t.stopPropagation();
          }
          moveHighlightBy(t, r) {
            if (!this.highlightedIndex) return;
            const o = this.colours,
              e = this.columns;
            let n = this.highlightedIndex % e,
              i = Math.floor(this.highlightedIndex / e);
            (n += t),
              (i += r),
              t < 0
                ? n < 0 && i > 0
                  ? ((n = e - 1), i--)
                  : n < 0 && (n = 0)
                : t > 0
                ? n > e - 1 && i < Math.floor(o.length / e) - 1
                  ? ((n = 0), i++)
                  : n > e - 1 && n--
                : r < 0
                ? i < 0 && (i = 0)
                : r > 0 &&
                  i > Math.floor(o.length / e) - 1 &&
                  (i = Math.floor(o.length / e) - 1);
            const l = this.picker.childNodes[i].childNodes[n],
              s = i * e + n;
            this.setHighlightedCell(l, s);
          }
          onMouseMove(t) {
            const r = t.target,
              o = r && Number(r.getAttribute("data-index"));
            null !== o &&
              o !== this.highlightedIndex &&
              this.setHighlightedCell(r, o);
          }
          onMouseEnter() {
            var t;
            null === (t = this.picker) ||
              void 0 === t ||
              t.focus({ preventScroll: !0 });
          }
          onMouseLeave() {
            var t;
            null === (t = this.picker) || void 0 === t || t.blur();
            const r = this.getHighlighted();
            r && n.utils.dom.removeClass(r, "blocklyColourHighlighted");
          }
          getHighlighted() {
            var t;
            if (!this.highlightedIndex) return null;
            const r = this.highlightedIndex % this.columns,
              o = Math.floor(this.highlightedIndex / this.columns),
              e =
                null === (t = this.picker) || void 0 === t
                  ? void 0
                  : t.childNodes[o];
            return e ? e.childNodes[r] : null;
          }
          setHighlightedCell(t, r) {
            const o = this.getHighlighted();
            o && n.utils.dom.removeClass(o, "blocklyColourHighlighted"),
              n.utils.dom.addClass(t, "blocklyColourHighlighted"),
              (this.highlightedIndex = r);
            const e = t.getAttribute("id");
            e &&
              this.picker &&
              n.utils.aria.setState(
                this.picker,
                n.utils.aria.State.ACTIVEDESCENDANT,
                e
              );
          }
          dropdownCreate() {
            const t = this.columns,
              r = this.colours,
              o = this.getValue(),
              e = document.createElement("table");
            (e.className = "blocklyColourTable"),
              (e.tabIndex = 0),
              (e.dir = "ltr"),
              n.utils.aria.setRole(e, n.utils.aria.Role.GRID),
              n.utils.aria.setState(e, n.utils.aria.State.EXPANDED, !0),
              n.utils.aria.setState(
                e,
                n.utils.aria.State.ROWCOUNT,
                Math.floor(r.length / t)
              ),
              n.utils.aria.setState(e, n.utils.aria.State.COLCOUNT, t);
            let i = null;
            for (let l = 0; l < r.length; l++) {
              l % t == 0 &&
                ((i = document.createElement("tr")),
                n.utils.aria.setRole(i, n.utils.aria.Role.ROW),
                e.appendChild(i));
              const s = document.createElement("td");
              i.appendChild(s),
                s.setAttribute("data-colour", r[l]),
                (s.title = this.titles[l] || r[l]),
                (s.id = n.utils.idGenerator.getNextUniqueId()),
                s.setAttribute("data-index", `${l}`),
                n.utils.aria.setRole(s, n.utils.aria.Role.GRIDCELL),
                n.utils.aria.setState(s, n.utils.aria.State.LABEL, r[l]),
                n.utils.aria.setState(
                  s,
                  n.utils.aria.State.SELECTED,
                  r[l] === o
                ),
                (s.style.backgroundColor = r[l]),
                r[l] === o &&
                  ((s.className = "blocklyColourSelected"),
                  (this.highlightedIndex = l));
            }
            this.boundEvents.push(
              n.browserEvents.conditionalBind(
                e,
                "pointerdown",
                this,
                this.onClick,
                !0
              )
            ),
              this.boundEvents.push(
                n.browserEvents.conditionalBind(
                  e,
                  "pointermove",
                  this,
                  this.onMouseMove,
                  !0
                )
              ),
              this.boundEvents.push(
                n.browserEvents.conditionalBind(
                  e,
                  "pointerenter",
                  this,
                  this.onMouseEnter,
                  !0
                )
              ),
              this.boundEvents.push(
                n.browserEvents.conditionalBind(
                  e,
                  "pointerleave",
                  this,
                  this.onMouseLeave,
                  !0
                )
              ),
              this.boundEvents.push(
                n.browserEvents.conditionalBind(
                  e,
                  "keydown",
                  this,
                  this.onKeyDown,
                  !1
                )
              ),
              (this.picker = e);
          }
          dropdownDispose() {
            for (const t of this.boundEvents) n.browserEvents.unbind(t);
            (this.boundEvents.length = 0),
              (this.picker = null),
              (this.highlightedIndex = null);
          }
          static fromJson(t) {
            return new this(t.colour, void 0, t);
          }
        }
        function l() {
          n.fieldRegistry.unregister("field_colour"),
            n.fieldRegistry.register("field_colour", i);
        }
        (i.prototype.DEFAULT_VALUE = "#ffffff"),
          n.Css.register(
            "\n.blocklyColourTable {\n  border-collapse: collapse;\n  display: block;\n  outline: none;\n  padding: 1px;\n}\n\n.blocklyColourTable>tr>td {\n  border: 0.5px solid #888;\n  box-sizing: border-box;\n  cursor: pointer;\n  display: inline-block;\n  height: 20px;\n  padding: 0;\n  width: 20px;\n}\n\n.blocklyColourTable>tr>td.blocklyColourHighlighted {\n  border-color: #eee;\n  box-shadow: 2px 2px 7px 2px rgba(0, 0, 0, 0.3);\n  position: relative;\n}\n\n.blocklyColourSelected, .blocklyColourSelected:hover {\n  border-color: #eee !important;\n  outline: 1px solid #333;\n  position: relative;\n}\n"
          );
        var s = a(127),
          u = a(379),
          d = a(157),
          h = a(537),
          g = a(557);
        const f = "colour_picker",
          O = {
            type: f,
            message0: "%1",
            args0: [
              { type: "field_colour", name: "COLOUR", colour: "#ff0000" },
            ],
            output: "Colour",
            helpUrl: "%{BKY_COLOUR_PICKER_HELPURL}",
            style: "colour_blocks",
            tooltip: "%{BKY_COLOUR_PICKER_TOOLTIP}",
            extensions: ["parent_tooltip_when_inline"],
          };
        function p(t, r) {
          return [r.quote_(t.getFieldValue("COLOUR")), s.Order.ATOMIC];
        }
        function b(t, r) {
          return [r.quote_(t.getFieldValue("COLOUR")), u.Order.ATOMIC];
        }
        function _(t, r) {
          return [r.quote_(t.getFieldValue("COLOUR")), d.Order.ATOMIC];
        }
        function C(t, r) {
          return [r.quote_(t.getFieldValue("COLOUR")), h.Order.ATOMIC];
        }
        function m(t, r) {
          return [r.quote_(t.getFieldValue("COLOUR")), g.Order.ATOMIC];
        }
        const E = n.common.createBlockDefinitionsFromJsonArray([O]),
          L = E[f];
        function N(t = {}) {
          l(),
            n.common.defineBlocks(E),
            t.javascript && (t.javascript.forBlock[f] = p),
            t.dart && (t.dart.forBlock[f] = b),
            t.lua && (t.lua.forBlock[f] = _),
            t.php && (t.php.forBlock[f] = C),
            t.python && (t.python.forBlock[f] = m);
        }
        const v = "colour_random",
          R = {
            type: v,
            message0: "%{BKY_COLOUR_RANDOM_TITLE}",
            output: "Colour",
            helpUrl: "%{BKY_COLOUR_RANDOM_HELPURL}",
            style: "colour_blocks",
            tooltip: "%{BKY_COLOUR_RANDOM_TOOLTIP}",
          };
        function $(t, r) {
          return [
            r.provideFunction_(
              "colourRandom",
              `\nfunction ${r.FUNCTION_NAME_PLACEHOLDER_}() {\n  var num = Math.floor(Math.random() * 0x1000000);\n  return '#' + ('00000' + num.toString(16)).substr(-6);\n}\n`
            ) + "()",
            s.Order.FUNCTION_CALL,
          ];
        }
        function T(t, r) {
          return (
            (r.definitions_.import_dart_math = "import 'dart:math' as Math;"),
            [
              r.provideFunction_(
                "colour_random",
                `\nString ${r.FUNCTION_NAME_PLACEHOLDER_}() {\n  String hex = '0123456789abcdef';\n  var rnd = new Math.Random();\n  return '#\${hex[rnd.nextInt(16)]}\${hex[rnd.nextInt(16)]}'\n      '\${hex[rnd.nextInt(16)]}\${hex[rnd.nextInt(16)]}'\n      '\${hex[rnd.nextInt(16)]}\${hex[rnd.nextInt(16)]}';\n}\n`
              ) + "()",
              u.Order.UNARY_POSTFIX,
            ]
          );
        }
        function x(t, r) {
          return [
            'string.format("#%06x", math.random(0, 2^24 - 1))',
            d.Order.HIGH,
          ];
        }
        function k(t, r) {
          return [
            r.provideFunction_(
              "colour_random",
              `\nfunction ${r.FUNCTION_NAME_PLACEHOLDER_}() {\n  return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);\n}\n`
            ) + "()",
            h.Order.FUNCTION_CALL,
          ];
        }
        function y(t, r) {
          return (
            (r.definitions_.import_random = "import random"),
            ["'#%06x' % random.randint(0, 2**24 - 1)", g.Order.FUNCTION_CALL]
          );
        }
        const B = n.common.createBlockDefinitionsFromJsonArray([R]),
          I = B[v];
        function U(t = {}) {
          l(),
            n.common.defineBlocks(B),
            t.javascript && (t.javascript.forBlock[v] = $),
            t.dart && (t.dart.forBlock[v] = T),
            t.lua && (t.lua.forBlock[v] = x),
            t.php && (t.php.forBlock[v] = k),
            t.python && (t.python.forBlock[v] = y);
        }
        const A = "colour_rgb",
          F = {
            type: A,
            message0:
              "%{BKY_COLOUR_RGB_TITLE} %{BKY_COLOUR_RGB_RED} %1 %{BKY_COLOUR_RGB_GREEN} %2 %{BKY_COLOUR_RGB_BLUE} %3",
            args0: [
              {
                type: "input_value",
                name: "RED",
                check: "Number",
                align: "RIGHT",
              },
              {
                type: "input_value",
                name: "GREEN",
                check: "Number",
                align: "RIGHT",
              },
              {
                type: "input_value",
                name: "BLUE",
                check: "Number",
                align: "RIGHT",
              },
            ],
            output: "Colour",
            helpUrl: "%{BKY_COLOUR_RGB_HELPURL}",
            style: "colour_blocks",
            tooltip: "%{BKY_COLOUR_RGB_TOOLTIP}",
          };
        function D(t, r) {
          const o = r.valueToCode(t, "RED", s.Order.NONE) || 0,
            e = r.valueToCode(t, "GREEN", s.Order.NONE) || 0,
            n = r.valueToCode(t, "BLUE", s.Order.NONE) || 0;
          return [
            `${r.provideFunction_(
              "colourRgb",
              `\nfunction ${r.FUNCTION_NAME_PLACEHOLDER_}(r, g, b) {\n  r = Math.max(Math.min(Number(r), 100), 0) * 2.55;\n  g = Math.max(Math.min(Number(g), 100), 0) * 2.55;\n  b = Math.max(Math.min(Number(b), 100), 0) * 2.55;\n  r = ('0' + (Math.round(r) || 0).toString(16)).slice(-2);\n  g = ('0' + (Math.round(g) || 0).toString(16)).slice(-2);\n  b = ('0' + (Math.round(b) || 0).toString(16)).slice(-2);\n  return '#' + r + g + b;\n}\n`
            )}(${o}, ${e}, ${n})`,
            s.Order.FUNCTION_CALL,
          ];
        }
        function S(t, r) {
          const o = r.valueToCode(t, "RED", u.Order.NONE) || 0,
            e = r.valueToCode(t, "GREEN", u.Order.NONE) || 0,
            n = r.valueToCode(t, "BLUE", u.Order.NONE) || 0;
          return (
            (r.definitions_.import_dart_math = "import 'dart:math' as Math;"),
            [
              `${r.provideFunction_(
                "colour_rgb",
                `\nString ${r.FUNCTION_NAME_PLACEHOLDER_}(num r, num g, num b) {\n  num rn = (Math.max(Math.min(r, 100), 0) * 2.55).round();\n  String rs = rn.toInt().toRadixString(16);\n  rs = '0$rs';\n  rs = rs.substring(rs.length - 2);\n  num gn = (Math.max(Math.min(g, 100), 0) * 2.55).round();\n  String gs = gn.toInt().toRadixString(16);\n  gs = '0$gs';\n  gs = gs.substring(gs.length - 2);\n  num bn = (Math.max(Math.min(b, 100), 0) * 2.55).round();\n  String bs = bn.toInt().toRadixString(16);\n  bs = '0$bs';\n  bs = bs.substring(bs.length - 2);\n  return '#$rs$gs$bs';\n}\n`
              )}(${o}, ${e}, ${n})`,
              u.Order.UNARY_POSTFIX,
            ]
          );
        }
        function M(t, r) {
          return [
            `${r.provideFunction_(
              "colour_rgb",
              `\nfunction ${r.FUNCTION_NAME_PLACEHOLDER_}(r, g, b)\n  r = math.floor(math.min(100, math.max(0, r)) * 2.55 + .5)\n  g = math.floor(math.min(100, math.max(0, g)) * 2.55 + .5)\n  b = math.floor(math.min(100, math.max(0, b)) * 2.55 + .5)\n  return string.format("#%02x%02x%02x", r, g, b)\nend\n`
            )}(${r.valueToCode(t, "RED", d.Order.NONE) || 0}, ${
              r.valueToCode(t, "GREEN", d.Order.NONE) || 0
            }, ${r.valueToCode(t, "BLUE", d.Order.NONE) || 0})`,
            d.Order.HIGH,
          ];
        }
        function P(t, r) {
          const o = r.valueToCode(t, "RED", h.Order.NONE) || 0,
            e = r.valueToCode(t, "GREEN", h.Order.NONE) || 0,
            n = r.valueToCode(t, "BLUE", h.Order.NONE) || 0;
          return [
            `${r.provideFunction_(
              "colour_rgb",
              `\nfunction ${r.FUNCTION_NAME_PLACEHOLDER_}($r, $g, $b) {\n  $r = round(max(min($r, 100), 0) * 2.55);\n  $g = round(max(min($g, 100), 0) * 2.55);\n  $b = round(max(min($b, 100), 0) * 2.55);\n  $hex = '#';\n  $hex .= str_pad(dechex($r), 2, '0', STR_PAD_LEFT);\n  $hex .= str_pad(dechex($g), 2, '0', STR_PAD_LEFT);\n  $hex .= str_pad(dechex($b), 2, '0', STR_PAD_LEFT);\n  return $hex;\n}\n`
            )}(${o}, ${e}, ${n})`,
            h.Order.FUNCTION_CALL,
          ];
        }
        function H(t, r) {
          return [
            r.provideFunction_(
              "colour_rgb",
              `\ndef ${r.FUNCTION_NAME_PLACEHOLDER_}(r, g, b):\n  r = round(min(100, max(0, r)) * 2.55)\n  g = round(min(100, max(0, g)) * 2.55)\n  b = round(min(100, max(0, b)) * 2.55)\n  return '#%02x%02x%02x' % (r, g, b)\n`
            ) +
              "(" +
              (r.valueToCode(t, "RED", g.Order.NONE) || 0) +
              ", " +
              (r.valueToCode(t, "GREEN", g.Order.NONE) || 0) +
              ", " +
              (r.valueToCode(t, "BLUE", g.Order.NONE) || 0) +
              ")",
            g.Order.FUNCTION_CALL,
          ];
        }
        const w = n.common.createBlockDefinitionsFromJsonArray([F]),
          G = w[A];
        function K(t = {}) {
          l(),
            n.common.defineBlocks(w),
            t.javascript && (t.javascript.forBlock[A] = D),
            t.dart &&
              ((t.dart.forBlock[A] = S), t.dart.addReservedWords("Math")),
            t.lua && (t.lua.forBlock[A] = M),
            t.php && (t.php.forBlock[A] = P),
            t.python && (t.python.forBlock[A] = H);
        }
        const j = "colour_blend",
          Y = {
            type: j,
            message0:
              "%{BKY_COLOUR_BLEND_TITLE} %{BKY_COLOUR_BLEND_COLOUR1} %1 %{BKY_COLOUR_BLEND_COLOUR2} %2 %{BKY_COLOUR_BLEND_RATIO} %3",
            args0: [
              {
                type: "input_value",
                name: "COLOUR1",
                check: "Colour",
                align: "RIGHT",
              },
              {
                type: "input_value",
                name: "COLOUR2",
                check: "Colour",
                align: "RIGHT",
              },
              {
                type: "input_value",
                name: "RATIO",
                check: "Number",
                align: "RIGHT",
              },
            ],
            output: "Colour",
            helpUrl: "%{BKY_COLOUR_BLEND_HELPURL}",
            style: "colour_blocks",
            tooltip: "%{BKY_COLOUR_BLEND_TOOLTIP}",
          };
        function q(t, r) {
          const o = r.valueToCode(t, "COLOUR1", s.Order.NONE) || "'#000000'",
            e = r.valueToCode(t, "COLOUR2", s.Order.NONE) || "'#000000'",
            n = r.valueToCode(t, "RATIO", s.Order.NONE) || 0.5;
          return [
            `${r.provideFunction_(
              "colourBlend",
              `\nfunction ${r.FUNCTION_NAME_PLACEHOLDER_}(c1, c2, ratio) {\n  ratio = Math.max(Math.min(Number(ratio), 1), 0);\n  var r1 = parseInt(c1.substring(1, 3), 16);\n  var g1 = parseInt(c1.substring(3, 5), 16);\n  var b1 = parseInt(c1.substring(5, 7), 16);\n  var r2 = parseInt(c2.substring(1, 3), 16);\n  var g2 = parseInt(c2.substring(3, 5), 16);\n  var b2 = parseInt(c2.substring(5, 7), 16);\n  var r = Math.round(r1 * (1 - ratio) + r2 * ratio);\n  var g = Math.round(g1 * (1 - ratio) + g2 * ratio);\n  var b = Math.round(b1 * (1 - ratio) + b2 * ratio);\n  r = ('0' + (r || 0).toString(16)).slice(-2);\n  g = ('0' + (g || 0).toString(16)).slice(-2);\n  b = ('0' + (b || 0).toString(16)).slice(-2);\n  return '#' + r + g + b;\n}\n`
            )}(${o}, ${e}, ${n})`,
            s.Order.FUNCTION_CALL,
          ];
        }
        function V(t, r) {
          const o = r.valueToCode(t, "COLOUR1", u.Order.NONE) || "'#000000'",
            e = r.valueToCode(t, "COLOUR2", u.Order.NONE) || "'#000000'",
            n = r.valueToCode(t, "RATIO", u.Order.NONE) || 0.5;
          return (
            (r.definitions_.import_dart_math = "import 'dart:math' as Math;"),
            [
              `${r.provideFunction_(
                "colour_blend",
                `\nString ${r.FUNCTION_NAME_PLACEHOLDER_}(String c1, String c2, num ratio) {\n  ratio = Math.max(Math.min(ratio, 1), 0);\n  int r1 = int.parse('0x\${c1.substring(1, 3)}');\n  int g1 = int.parse('0x\${c1.substring(3, 5)}');\n  int b1 = int.parse('0x\${c1.substring(5, 7)}');\n  int r2 = int.parse('0x\${c2.substring(1, 3)}');\n  int g2 = int.parse('0x\${c2.substring(3, 5)}');\n  int b2 = int.parse('0x\${c2.substring(5, 7)}');\n  num rn = (r1 * (1 - ratio) + r2 * ratio).round();\n  String rs = rn.toInt().toRadixString(16);\n  num gn = (g1 * (1 - ratio) + g2 * ratio).round();\n  String gs = gn.toInt().toRadixString(16);\n  num bn = (b1 * (1 - ratio) + b2 * ratio).round();\n  String bs = bn.toInt().toRadixString(16);\n  rs = '0$rs';\n  rs = rs.substring(rs.length - 2);\n  gs = '0$gs';\n  gs = gs.substring(gs.length - 2);\n  bs = '0$bs';\n  bs = bs.substring(bs.length - 2);\n  return '#$rs$gs$bs';\n}\n`
              )}(${o}, ${e}, ${n})`,
              u.Order.UNARY_POSTFIX,
            ]
          );
        }
        function J(t, r) {
          return [
            `${r.provideFunction_(
              "colour_blend",
              `\nfunction ${r.FUNCTION_NAME_PLACEHOLDER_}(colour1, colour2, ratio)\n  local r1 = tonumber(string.sub(colour1, 2, 3), 16)\n  local r2 = tonumber(string.sub(colour2, 2, 3), 16)\n  local g1 = tonumber(string.sub(colour1, 4, 5), 16)\n  local g2 = tonumber(string.sub(colour2, 4, 5), 16)\n  local b1 = tonumber(string.sub(colour1, 6, 7), 16)\n  local b2 = tonumber(string.sub(colour2, 6, 7), 16)\n  local ratio = math.min(1, math.max(0, ratio))\n  local r = math.floor(r1 * (1 - ratio) + r2 * ratio + .5)\n  local g = math.floor(g1 * (1 - ratio) + g2 * ratio + .5)\n  local b = math.floor(b1 * (1 - ratio) + b2 * ratio + .5)\n  return string.format("#%02x%02x%02x", r, g, b)\nend\n`
            )}(${r.valueToCode(t, "COLOUR1", d.Order.NONE) || "'#000000'"}, ${
              r.valueToCode(t, "COLOUR2", d.Order.NONE) || "'#000000'"
            }, ${r.valueToCode(t, "RATIO", d.Order.NONE) || 0})`,
            d.Order.HIGH,
          ];
        }
        function z(t, r) {
          const o = r.valueToCode(t, "COLOUR1", h.Order.NONE) || "'#000000'",
            e = r.valueToCode(t, "COLOUR2", h.Order.NONE) || "'#000000'",
            n = r.valueToCode(t, "RATIO", h.Order.NONE) || 0.5;
          return [
            `${r.provideFunction_(
              "colour_blend",
              `\nfunction ${r.FUNCTION_NAME_PLACEHOLDER_}($c1, $c2, $ratio) {\n  $ratio = max(min($ratio, 1), 0);\n  $r1 = hexdec(substr($c1, 1, 2));\n  $g1 = hexdec(substr($c1, 3, 2));\n  $b1 = hexdec(substr($c1, 5, 2));\n  $r2 = hexdec(substr($c2, 1, 2));\n  $g2 = hexdec(substr($c2, 3, 2));\n  $b2 = hexdec(substr($c2, 5, 2));\n  $r = round($r1 * (1 - $ratio) + $r2 * $ratio);\n  $g = round($g1 * (1 - $ratio) + $g2 * $ratio);\n  $b = round($b1 * (1 - $ratio) + $b2 * $ratio);\n  $hex = '#';\n  $hex .= str_pad(dechex($r), 2, '0', STR_PAD_LEFT);\n  $hex .= str_pad(dechex($g), 2, '0', STR_PAD_LEFT);\n  $hex .= str_pad(dechex($b), 2, '0', STR_PAD_LEFT);\n  return $hex;\n}\n`
            )}(${o}, ${e}, ${n})`,
            h.Order.FUNCTION_CALL,
          ];
        }
        function W(t, r) {
          return [
            `${r.provideFunction_(
              "colour_blend",
              `\ndef ${r.FUNCTION_NAME_PLACEHOLDER_}(colour1, colour2, ratio):\n  r1, r2 = int(colour1[1:3], 16), int(colour2[1:3], 16)\n  g1, g2 = int(colour1[3:5], 16), int(colour2[3:5], 16)\n  b1, b2 = int(colour1[5:7], 16), int(colour2[5:7], 16)\n  ratio = min(1, max(0, ratio))\n  r = round(r1 * (1 - ratio) + r2 * ratio)\n  g = round(g1 * (1 - ratio) + g2 * ratio)\n  b = round(b1 * (1 - ratio) + b2 * ratio)\n  return '#%02x%02x%02x' % (r, g, b)\n`
            )}(${r.valueToCode(t, "COLOUR1", g.Order.NONE) || "'#000000'"}, ${
              r.valueToCode(t, "COLOUR2", g.Order.NONE) || "'#000000'"
            }, ${r.valueToCode(t, "RATIO", g.Order.NONE) || 0})`,
            g.Order.FUNCTION_CALL,
          ];
        }
        const X = n.common.createBlockDefinitionsFromJsonArray([Y]),
          Z = X[j];
        function Q(t = {}) {
          l(),
            n.common.defineBlocks(X),
            t.javascript && (t.javascript.forBlock[j] = q),
            t.dart &&
              ((t.dart.forBlock[j] = V), t.dart.addReservedWords("Math")),
            t.lua && (t.lua.forBlock[j] = J),
            t.php && (t.php.forBlock[j] = z),
            t.python && (t.python.forBlock[j] = W);
        }
        function tt(t = {}) {
          N(t), K(t), U(t), Q(t);
        }
      })(),
      c
    );
  })()
);
